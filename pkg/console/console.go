package console

import (
	"fmt"
	"github.com/mgutz/ansi"
	"os"
)

// Success 打印一条成功消息，绿色输出
func Success(msg string)  {
	colorOut(msg, "green")
}

func Error(msg string)  {
	colorOut(msg, "red")
}

func Warning(msg string)  {
	colorOut(msg, "yellow")
}

func Exit(msg string)  {
	Error(msg)
	os.Exit(1)
}

// ExitIf 语法糖，自带 err != nil 判断
func ExitIf(err error)  {
	if err != nil {
		Exit(err.Error())
	}
}

// colorOut 内部使用，设置高亮颜色
func colorOut(message, color string)  {
	_, _ = fmt.Fprintln(os.Stdout, ansi.Color(message, color))
}

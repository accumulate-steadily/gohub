package verifycode

import (
	"gohub/pkg/app"
	"gohub/pkg/config"
	"gohub/pkg/redis"
	"time"
)

type RedisStore struct {
	RedisClient *redis.RedisClient
	KeyPrefix   string
}

func (s *RedisStore) Set(id string, value string) bool {
	ExpireTime := time.Minute * time.Duration(config.GetInt64("verifycode.expire_time"))
	if app.IsLocal() {
		ExpireTime = time.Duration(config.GetInt64("verifycode.debug_expire_time"))
	}
	return s.RedisClient.Set(s.KeyPrefix+id, value, ExpireTime)
}

func (s *RedisStore) Get(id string, clear bool) (value string) {
	value = s.RedisClient.Get(s.KeyPrefix + id)
	if clear {
		s.RedisClient.Del(s.KeyPrefix + id)
	}
	return
}

func (s *RedisStore) Verify(id, answer string, clear bool) bool {
	v := s.RedisClient.Get(id)
	return v == answer
}

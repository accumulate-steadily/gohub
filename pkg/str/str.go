package str

import (
	"github.com/gertd/go-pluralize"
	"github.com/iancoleman/strcase"
)

// Plural 转为复数 user -> users
func Plural(word string) string {
	return pluralize.NewClient().Plural(word)
}

// Singular 转为单数， users -> user
func Singular(words string) string {
	return pluralize.NewClient().Singular(words)
}

// Snake 转为 snake_case，如 TopicComment -> topic_comment
func Snake(s string) string {
	return strcase.ToSnake(s)
}

// 大驼峰 topic_comment -> TopicComment
func Camel(s string) string {
	return strcase.ToCamel(s)
}

// 小驼峰 TopicComment -> topicComment
func LowerCamel(s string) string {
	return strcase.ToLowerCamel(s)
}
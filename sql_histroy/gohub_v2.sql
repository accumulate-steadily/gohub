/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : gohub_v2

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 01/04/2023 22:30:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(3) NULL DEFAULT NULL,
  `updated_at` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_categories_created_at`(`created_at`) USING BTREE,
  INDEX `idx_categories_updated_at`(`updated_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'java', '世界赏最好的语言2', '2023-03-28 20:44:18.949', '2023-03-28 20:45:25.428');
INSERT INTO `categories` VALUES (2, 'rust', '世界赏最好的语言', '2023-04-01 17:03:24.182', '2023-04-01 17:03:24.182');
INSERT INTO `categories` VALUES (3, 'php', '世界赏最好的语言', '2023-04-01 17:03:30.073', '2023-04-01 17:03:30.073');

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(3) NULL DEFAULT NULL,
  `updated_at` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_categories_created_at`(`created_at`) USING BTREE,
  INDEX `idx_categories_updated_at`(`updated_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of links
-- ----------------------------
INSERT INTO `links` VALUES (2, '知乎', 'www.zhihu.com', '2023-04-01 17:29:24.882', '2023-04-01 17:29:24.882');

-- ----------------------------
-- Table structure for topics
-- ----------------------------
DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `created_at` datetime(3) NULL DEFAULT NULL,
  `updated_at` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_categories_created_at`(`created_at`) USING BTREE,
  INDEX `idx_categories_updated_at`(`updated_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of topics
-- ----------------------------
INSERT INTO `topics` VALUES (2, '我发的帖子2', '这里是帖子描述这里是帖子描述2', 5, 1, '2023-04-01 17:04:01.730', '2023-04-01 17:04:01.730');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `email` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `phone` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `password` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_at` datetime(3) NULL DEFAULT NULL,
  `updated_at` datetime(3) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_users_updated_at`(`updated_at`) USING BTREE,
  INDEX `idx_users_created_at`(`created_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (5, 'summer', 'summary5@testing.com', '', '$2a$14$qPAWubf7z6phRjZhL65qMe801feV.sAeuJWHoTqeLTLI1RgFHxC3e', '2023-03-27 16:53:46.470', '2023-03-27 16:53:46.470', '', '', '');
INSERT INTO `users` VALUES (6, 'summer1', 'summary1@testing.com', '', '$2a$14$qPAWubf7z6phRjZhL65qMe801feV.sAeuJWHoTqeLTLI1RgFHxC3e', '2023-03-27 16:53:46.470', '2023-03-27 16:53:46.470', '', '', '');
INSERT INTO `users` VALUES (7, 'summer2', 'summary2@testing.com', '', '$2a$14$qPAWubf7z6phRjZhL65qMe801feV.sAeuJWHoTqeLTLI1RgFHxC3e', '2023-03-27 16:53:46.470', '2023-03-27 16:53:46.470', '', '', '');
INSERT INTO `users` VALUES (8, 'summer3', 'summary3@testing.com', '', '$2a$14$qPAWubf7z6phRjZhL65qMe801feV.sAeuJWHoTqeLTLI1RgFHxC3e', '2023-03-27 16:53:46.470', '2023-03-27 16:53:46.470', '', '', '');
INSERT INTO `users` VALUES (9, 'summer4', 'summary3@testing.com', '', '$2a$14$qPAWubf7z6phRjZhL65qMe801feV.sAeuJWHoTqeLTLI1RgFHxC3e', '2023-03-27 16:53:46.470', '2023-03-27 16:53:46.470', '', '', '');

SET FOREIGN_KEY_CHECKS = 1;

package config

import (
	"fmt"
	"gohub/pkg/config"
	"testing"
)

func Test_config(t *testing.T) {
	// 需要将里面的env 路径修改为 ../.env 才能调试通
	config.InitConfig("")
	fmt.Println(config.Get("app.port"))
}

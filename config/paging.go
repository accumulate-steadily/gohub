package config

import "gohub/pkg/config"

func init()  {

	config.Add("paging", func() map[string]interface{} {

		return map[string]interface{}{

			// 默认每页条数
			"perpage" : 2,

			// 分页参数：多少页
			"url_query_page" : "page",

			// 分页参数：排序字段
			"url_query_sort": "sort",

			// 排序规则：
			"url_query_order": "order",

			// 每页条数
			"url_query_per_page": "per_page",
		}
	})
}
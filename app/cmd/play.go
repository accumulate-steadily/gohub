package cmd

import (
	"github.com/spf13/cobra"
	"gohub/pkg/console"
	"gohub/pkg/redis"
	"time"
)

// 用于临时调试代码

var CmdPlay = &cobra.Command{
	Use: "play",
	Short: "Likes the Go Playground, but running at our application context",
	Run: runPlay,
}

// 调试完成后，请记得清除测试代码
func runPlay(cmd *cobra.Command, args []string)  {

	// 存入 redis 中
	redis.Redis.Set("Hello", "hi from redis", 10 * time.Second)

	// 从 redis 中取出
	console.Success(redis.Redis.Get("Hello"))
}

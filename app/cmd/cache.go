package cmd

import (
    "fmt"
    "github.com/spf13/cobra"
    "gohub/pkg/cache"
    "gohub/pkg/console"
)

var CmdCache = &cobra.Command{
    Use:   "cache",
    Short:  "Cache management",
}

var CmdCacheClear = &cobra.Command{
    Use: "clear",
    Short: "clear cache",
    Run: func(cmd *cobra.Command, args []string) {
        cache.Flush()
        console.Success("Cache cleared.")
    },
}


// cacheKey 命令行选项
var cacheKey string

func init()  {
    CmdCache.AddCommand(CmdCacheClear, CmdCacheForget)

    // 设置 cache forget 命令的选项
    CmdCacheForget.Flags().StringVarP(&cacheKey, "key", "k", "", "Key of the cache")
    _ = CmdCacheForget.MarkFlagRequired("key")
}

var CmdCacheForget = &cobra.Command{
    Use: "forget",
    Short: "Delete redis key, example: cache forget cache-key",
    Run: func(cmd *cobra.Command, args []string) {
        cache.Forget(cacheKey)
        console.Success(fmt.Sprintf("Cache key [%s] deleted.", cacheKey))
    },
}

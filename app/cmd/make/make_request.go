package make

import (
	"fmt"
	"github.com/spf13/cobra"
)

var CmdMakeRequest = &cobra.Command{
	Use: "request",
	Short: "example: make request user",
	Run: func(cmd *cobra.Command, args []string) {

		// 格式化模型名称，返回一个model对象
		model := makeModelFromString(args[0])

		// 拼接目录文件路径
		filePath := fmt.Sprintf("app/requests/%s_request.go", model.PackageName)

		// 基于模板创建文件
		createFileFromStub(filePath, "request", model)
	},
	Args: cobra.ExactArgs(1),
}

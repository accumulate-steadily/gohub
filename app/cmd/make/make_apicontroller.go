package make

import (
	"fmt"
	"github.com/spf13/cobra"
	"gohub/pkg/console"
	"strings"
)

var CmdMakeAPIController = &cobra.Command{
	Use: "apicontroller",
	Short: "example: make apicontroller v1/user",
	Run: func(cmd *cobra.Command, args []string) {

		// 处理参数，要求附带 API 版本（v1 或者 v2）
		array := strings.Split(args[0], "/")
		if len(array) != 2 {
			console.Exit("api controller name format: v1/user")
		}

		// apiVersion 用来拼接目标路径
		// name 用来生成 cmd.Model 实例
		apiVersion, name := array[0], array[1]
		model := makeModelFromString(name)

		// 组件目标路径
		filePath := fmt.Sprintf("app/http/controllers/api/%s/%s_controller.go", apiVersion, model.TableName)

		// 基于模板创建文件
		createFileFromStub(filePath, "apicontroller", model)
	},
	Args: cobra.ExactArgs(1),
}

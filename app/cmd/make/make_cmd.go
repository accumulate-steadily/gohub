package make

import (
	"fmt"
	"github.com/spf13/cobra"
	"gohub/pkg/console"
)

var CmdMakeCMD = &cobra.Command{
	Use: "cmd",
	Short: "创建一个命令，should be sname_case, example: mame cmd buckup_database",
	Run: func(cmd *cobra.Command, args []string) {

		// 格式化模型名称， 返回衣蛾 Model 对象
		model := makeModelFromString(args[0])

		// 拼接目标文件路径
		filepath := fmt.Sprintf("app/cmd/%s.go", model.PackageName)

		createFileFromStub(filepath, "cmd", model)

		// 友好提示
		console.Success("command name: " + model.PackageName)
		console.Success("command variable name: cmd.Cmd" + model.StructName)
		console.Warning("please edit main.go's app.Commands slice to register command")
	},
	Args:cobra.ExactArgs(1), // 只允许一且必须传 1 个参数
}

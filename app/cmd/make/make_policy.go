package make

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var CmdMakePolicy = &cobra.Command{
	Use:   "policy",
	Short: "example: make policy user",
	Run: func(cmd *cobra.Command, args []string) {

		model := makeModelFromString(args[0])

		_ = os.MkdirAll("app/policies", os.ModePerm)

		filePath := fmt.Sprintf("app/policies/%s_policy.go", model.PackageName)

		createFileFromStub(filePath, "policy", model)
	},
	Args: cobra.ExactArgs(1),
}

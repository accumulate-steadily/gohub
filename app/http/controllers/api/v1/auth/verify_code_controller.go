package auth

import (
	"github.com/gin-gonic/gin"
	v1 "gohub/app/http/controllers/api/v1"
	"gohub/app/requests"
	"gohub/pkg/captcha"
	"gohub/pkg/logger"
	"gohub/pkg/response"
	"gohub/pkg/verifycode"
)

// VerifyCodeController 用户控制器
type VerifyCodeController struct {
	v1.BaseAPIController
}

// ShowCaptcha 显示图片验证码
func (vc *VerifyCodeController) ShowCaptcha(c *gin.Context) {

	// 生成验证码
	id, b64s, err := captcha.NewCaptcha().GenerateCaptcha()
	logger.LogIf(err)

	response.JSON(c, gin.H{
		"captcha_id":    id,
		"captcha_image": b64s,
	})
}

// SendUsingPhone 发送短信验证码
func (vc *VerifyCodeController) SendUsingPhone(c *gin.Context) {

	request := requests.VerifyCodePhoneRequest{}
	if ok := requests.ToValidate(c, &request, requests.VerifyCodePhone); !ok {
		return
	}

	// todo 增加发送频繁的限制

	if ok := verifycode.NewVerifyCode().SendSMS(request.Phone); !ok {
		response.Abort500(c, "发送短信错误")
	} else {
		response.Success(c)
	}
}

// SendUsingEmail 发送邮件验证码
func (vc *VerifyCodeController) SendUsingEmail(c *gin.Context) {

	request := requests.VerifyCodeEmailRequest{}
	if ok := requests.ToValidate(c, &request, requests.VerifyCodeEmail); !ok {
		return
	}

	// todo 增加发送频繁的限制

	if err := verifycode.NewVerifyCode().SendEmail(request.Email); err != nil {
		response.Abort500(c, "发送 Email 验证码失败~")
	} else {
		response.Success(c)
	}
}

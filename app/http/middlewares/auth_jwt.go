package middlewares

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gohub/app/models/user"
	"gohub/pkg/config"
	"gohub/pkg/jwt"
	"gohub/pkg/logger"
	"gohub/pkg/response"
)

func AuthJWT() gin.HandlerFunc {

	return func(c *gin.Context) {

		// 从标头 Authorization:Bearer xxxxx 中获取信息，并验证 JWT 的准确性
		claims, err := jwt.NewJWT().ParserToken(c)

		if err != nil {
			logger.DebugString("JWT", "解析token", err.Error())
			response.Unauthorized(c, fmt.Sprintf("请查询 %v 相关认证文档", config.GetString("app.name")))
			return
		}

		userMode := user.Get(claims.UserId)
		if userMode.ID == 0 {
			response.Unauthorized(c, "找不到对应用户，用户可能已删除")
			return
		}

		// 将用户信息存入 gin.context 中，后续 auth 包将从这里拿到当前用户数据
		c.Set("current_user_id", userMode.GetStringID())
		c.Set("current_user_name", userMode.Name)
		c.Set("current_user", userMode)

		c.Next()
	}
}

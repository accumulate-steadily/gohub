package user

import (
	"github.com/gin-gonic/gin"
	"gohub/pkg/app"
	"gohub/pkg/database"
	"gohub/pkg/paginator"
)

// IsEmailExist 判断 Email 是否存在
func IsEmailExist(email string) bool {
	var count int64
	database.DB.Model(User{}).Where("email = ?", email).Count(&count)
	return count > 0
}

func IsPhoneExist(phone string) bool {
	var count int64
	database.DB.Model(User{}).Where("phone = ?", phone).Count(&count)
	return count > 0
}

// Get 通过手机号来获取用户
func Get(ID string) (userModel User) {
	database.DB.Where("id=?", ID).First(&userModel)
	return
}

// GetByPhone 通过手机号来获取用户
func GetByPhone(phone string) (userModel User) {
	database.DB.Where("phone=?", phone).First(&userModel)
	return
}

// GetByEmail 通过手机号来获取用户
func GetByEmail(email string) (userModel User) {
	database.DB.Where("email=?", email).First(&userModel)
	return
}

// GetByMulti 通过手机号/用户名/Email 来获取用户
func GetByMulti(loginId string) (userModel User) {
	database.DB.Where("phone = ?", loginId).
		Or("email = ?", loginId).
		Or("name = ?", loginId).
		First(&userModel)

	return
}

// All 获取所有用户数据
func All() (users []User) {
	database.DB.Find(&users)
	return
}

// Paginate 分页内容
func Paginate(c *gin.Context, perPage int) (users []User, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(&User{}),
		&users,
		app.V1URL(database.TableName(&User{})),
		perPage,
	)
	return
}

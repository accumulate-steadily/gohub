package link

import (
	"gohub/app/models"
	"gohub/pkg/database"
)

type Link struct {
	models.BaseModel

	Name string `json:"name,omitempty"`
	URL  string `json:"url,omitempty"`

	models.CommonTimestampsField
}

func (model *Link) Create() {
	database.DB.Create(&model)
}

func (model *Link) Save() (rowsAffected int64) {
	result := database.DB.Save(&model)
	return result.RowsAffected
}

func (model *Link) Delete() (rowsAffected int64)  {
	result := database.DB.Delete(&model)
	return result.RowsAffected
}

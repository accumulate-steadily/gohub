package category

import (
	"github.com/gin-gonic/gin"
	"gohub/pkg/app"
	"gohub/pkg/database"
	"gohub/pkg/paginator"
)


// Get 通过手机号来获取用户
func Get(ID string) (CategoryModel Category) {
	database.DB.Where("id=?", ID).First(&CategoryModel)
	return
}

// All 获取所有用户数据
func All() (Categorys []Category) {
	database.DB.Find(&Categorys)
	return
}

// Paginate 分页内容
func Paginate(c *gin.Context, perPage int) (Categorys []Category, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(&Category{}),
		&Categorys,
		app.V1URL(database.TableName(&Category{})),
		perPage,
	)
	return
}

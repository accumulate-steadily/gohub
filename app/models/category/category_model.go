package category

import (
	"gohub/app/models"
	"gohub/pkg/database"
)

type Category struct {
	models.BaseModel

	Name string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`

	models.CommonTimestampsField
}


func (categoryModel *Category) Create() {
	database.DB.Create(&categoryModel)
}

func (categoryModel *Category) Save() (rowsAffected int64) {
	result := database.DB.Save(&categoryModel)
	return result.RowsAffected
}

func (categoryModel *Category) Delete() (rowsAffected int64)  {
	result := database.DB.Delete(&categoryModel)
	return result.RowsAffected
}
package topic

import (
	"github.com/gin-gonic/gin"
	"gohub/pkg/app"
	"gohub/pkg/database"
	"gohub/pkg/paginator"
	"gorm.io/gorm/clause"
)

// Get 通过手机号来获取用户
func Get(ID string) (topic Topic) {
	//database.DB.Where("id=?", ID).First(&TopicsModel)

	// 附带 user 和 category 的关联信息
	database.DB.Preload(clause.Associations).Where("id=?", ID).First(&topic)

	return
}

// All 获取所有用户数据
func All() (Topics []Topic) {
	database.DB.Find(&Topics)
	return
}

// Paginate 分页内容
func Paginate(c *gin.Context, perPage int) (Topics []Topic, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(&Topic{}),
		&Topics,
		app.V1URL(database.TableName(&Topic{})),
		perPage,
	)
	return
}
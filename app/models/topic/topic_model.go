package topic

import (
	"gohub/app/models"
	"gohub/app/models/category"
	"gohub/app/models/user"
	"gohub/pkg/database"
)

type Topic struct {
	models.BaseModel

	Title      string `json:"title,omitempty"`
	Body       string `json:"body, omitempty"`
	UserId     string `json:"user_id,omitempty"`
	CategoryId string `json:"category_id,omitempty"`

	// 通过用户 user_id 关联
	User user.User `json:"user"`

	// 通过category_id 关联分类
	Category category.Category `json:"category"`

	models.CommonTimestampsField
}


func (topicModel *Topic) Create() {
	database.DB.Create(&topicModel)
}

func (topicModel *Topic) Save() (rowsAffected int64) {
	result := database.DB.Save(&topicModel)
	return result.RowsAffected
}

func (topicModel *Topic) Delete() (rowsAffected int64)  {
	result := database.DB.Delete(&topicModel)
	return result.RowsAffected
}
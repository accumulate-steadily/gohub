package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
	"gohub/app/requests/validators"
)

type LoginPhoneRequest struct {
	Phone      string `json:"phone,omitempty" valid:"phone"`
	VerifyCode string `json:"verify_code,omitempty" valid:"verify_code"`
}

// ValidateLoginPhone 验证表单，返回长度等于零即通过
func ValidateLoginPhone(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"phone":       []string{"required", "digits:11"},
		"verify_code": []string{"required", "digits:6"},
	}

	messages := govalidator.MapData{
		"phone": []string{"required", "digits:11", "not_exists:users,phone"},
		"verify_code": []string{
			"required:验证码答案必填",
			"digits:验证码长度必须为 6 位的数字",
		},
	}

	errs := validate(data, rules, messages)

	// 手机验证
	_data := data.(*LoginPhoneRequest)
	errs = validators.ValidateVerifyCode(_data.Phone, _data.VerifyCode, errs)

	return errs
}

type LoginByPasswordRequest struct {
	LoginId string `json:"login_id,omitempty" valid:"login_id"`
	Password        string `json:"password,omitempty" valid:"password"`
	CaptchaId     string `json:"captcha_id,omitemtpy" valid:"captcha_id"`
	CaptchaAnswer string `json:"captcha_answer,omitempty" valid:"captcha_answer"`
}

func ValidateLoginByPassword(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"login_id": []string{"required", "min:3"},
		"password": []string{"required", "min:6"},

		"captcha_id":     []string{"required"},
		"captcha_answer": []string{"required", "digits:6"},
	}

	messages := govalidator.MapData{
		"login_id": []string{
			"required: 账号为必填项，参数名称 login_id",
			"digits: 长度至少 3 位",
		},
		"password": []string{
			"required:密码为必填项",
			"min:密码长度需大于 6",
		},

		"captcha_id": []string{
			"required:图片验证码的 ID 为必填",
		},
		"captcha_answer": []string{
			"required:图片验证码答案必填",
			"digits:图片验证码的长度必须为6位的数字",
		},
	}

	_data := data.(*LoginByPasswordRequest)
	errs := validate(_data, rules, messages)

	// 图片验证码
	errs = validators.ValidateCaptcha(_data.CaptchaId, _data.CaptchaAnswer, errs)

	return errs
}

package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type LinkRequest struct {
	Name string `valid:"name" json:"name,omitempty"`
	URL  string `valid:"url" json:"url,omitempty"`
}

func ValidateLinkSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"name": []string{"required", "min_cn:2", "max_cn:8", "not_exists:categories,name"},
		"url":  []string{"required", "min_cn:2", "max:255"},
	}
	messages := govalidator.MapData{
		"name": []string{
			"required:名称为必填项",
			"min_cn:名称长度需至少 2 个字",
			"max_cn:名称长度不能超过 8 个字",
			"not_exists:名称已存在",
		},
		"url": []string{
			"required:链接地址 为必填项",
			"min_cn:链接地址 长度需至少 2 个字",
			"max:链接地址 长度不能超过 255 个字",
		},
	}
	return validate(data, rules, messages)
}
